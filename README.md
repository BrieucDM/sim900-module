# README #
Brieuc du Maugouër
brieuc.dumaugouer@gmail.com

This python module can be used to control a sim900 GSM module from a Raspberry Pi.

To you use it you must first install the RPi.GPIO python package and disable the serial console.

This module is intended to be used with this board https://hackaday.io/project/4070-raspberry-alarm-upgrade