#SimCom Sim900 GSM modem library
import serial
import RPi.GPIO as GPIO
import time

#Initialize the library

#Initialize GPIOs
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(7,GPIO.OUT)

#Initialize the serial port
port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=1.0)


#-------------------------------------------------------------------------------
#Temporization routine
def __wait(time_lapse):
	time_start = time.time()
	time_end = (time_start + time_lapse)

	while time_end > time.time():
		pass


#-------------------------------------------------------------------------------
#Turn the GSM modem On if not already
def sim900_On():
    #Check modem state
    port.flushInput()
    port.write("AT\r")
    rcv = port.read(2)
    if len(rcv) != 0 :
        return 1

    #Turn On modem
    GPIO.output(7,0)
    time.sleep(0.1)
    GPIO.output(7,1)
    time.sleep(1)
    GPIO.output(7,0)
    time.sleep(5)

    #Check again modem state
    port.flushInput()
    port.write("AT\r")
    rcv = port.read(2)
    if len(rcv) == 0 :
        raise IOError("could not turn module on")
        return 0

    return 1


#-------------------------------------------------------------------------------
#Turn the GSM modem Off if not already
def sim900_Off():
    #Check modem state
    port.flushInput()
    port.write("AT\r")
    rcv = port.read(2)
    if len(rcv) == 0 :
        return 1

    #Turn Off modem
    GPIO.output(7,0)
    time.sleep(0.1)
    GPIO.output(7,1)
    time.sleep(1)
    GPIO.output(7,0)
    time.sleep(5)

    #Check again modem state
    port.flushInput()
    port.write("AT\r")
    rcv = port.read(2)
    if len(rcv) != 0 :
        raise IOError("could not turn module off")
        return 0

    return 1


#-------------------------------------------------------------------------------
#return 1 if the GSM modem is On, 0 otherwise
def is_sim900_On():
    #Check modem state
    port.flushInput()
    port.write("AT\r")
    rcv = port.read(2)
    if len(rcv) == 0 :
            return 0

    return 1


#-------------------------------------------------------------------------------
#send a sms to the given number
def sim900_SendSMS(dest,msg):
    #make sure the modem is turned on
    sim900_On()

    #register on the network
    port.flushInput()
    port.write("AT+CREG=1\r")
    rcv = port.read(20)
    res = rcv.find("OK",0,len(rcv))

    if res == -1 :
        raise IOError ("could not register modem (%s)"%rcv)
        return 0

    #select text mode
    port.flushInput()
    port.write("AT+CMGF=1\r")
    rcv = port.read(20)
    res = rcv.find("OK",0,len(rcv))

    if res == -1 :
        raise IOError ("could not select text mode (%s)"%rcv)
        return 0

    #enter sms mode
    port.flushInput()
    port.write("AT+CMGS=\""+dest+"\"\r")
    rcv = port.read(30)
    res = rcv.find(">",0,len(rcv))

    if res == -1 :
        raise IOError ("could not enter sms mode (%s)"%rcv)
        return 0

    #write message
    port.write(msg)

    #send sms
    port.flushInput()
    port.write(chr(26))
    __wait(5)
    rcv = port.read(100)
    res = rcv.find("OK",0,len(rcv))

    if res == -1 :
        raise IOError ("could not send sms (%s)"%rcv)
        return 0

    return 1


#-------------------------------------------------------------------------------
#call the given number
def sim900_Call(dest):
    #make sure the modem is turned on
    sim900_On()

    #register on the network
    port.flushInput()
    port.write("AT+CREG=1\r")
    rcv = port.read(20)
    res = rcv.find("OK",0,len(rcv))

    if res == -1 :
        raise IOError ("could not register modem (%s)"%rcv)
        return 0

    #call
    port.flushInput()
    port.write("ATD"+dest+";\r")
    rcv = port.read(30)
    res = rcv.find("OK",0,len(rcv))

    if res == -1 :
        raise IOError ("could not call (%s)"%rcv)
        return 0

    return 1


#-------------------------------------------------------------------------------
#Hang up a call
def sim900_Hangup():

    #call
    port.flushInput()
    port.write("ATH\r")
    rcv = port.read(10)
    res = rcv.find("OK",0,len(rcv))

    if res == -1 :
        raise IOError ("could not hang up(%s)"%rcv)
        return 0

    return 1


#-------------------------------------------------------------------------------
#get signal quality, under 10 is not good
def sim900_Signal():
    #make sure the modem is turned on
    sim900_On()

    #call
    port.flushInput()
    port.write("AT+CSQ\r")
    rcv = port.read(30)
    res = rcv.find(",",0,len(rcv))

    if res == -1 :
        raise IOError ("could not get signal status(%s)"%rcv)
        return 0

    signal = int(rcv[res-2:res])

    return signal
